import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Switch, Link, Route } from 'react-router-dom'
import * as Ethereum from './services/Ethereum'
import styles from './App.module.css'
import MediumEditor from 'medium-editor'
import 'medium-editor/dist/css/medium-editor.css'
import 'medium-editor/dist/css/themes/default.css'

import $ from "jquery"
import { useParams } from 'react-router'


//A implémenter en plus : articles qui existent déjà ?
//Editer à partir de ce qui existait déjà. Reload à la main après modification d'article


/**
* Fonction utilitaire appelant smart contrat pour ajouter un article
*/
const writeArticle = (methods, title, content) => {
	methods.writeArticle(title, content).send();
}

/**
* Permet d'afficher la page pour écrire de nouveaux articles
*/
const NewArticle = () => {
  const [editor, setEditor] = useState(null)
  useEffect(() => {setEditor(new MediumEditor(`.${styles.editable}`))}, [setEditor]);
  const contract = useSelector(({ contract }) => contract)
  useEffect(() => {
    if (contract) {
    	const methodes = contract.methods;
  	$("form").on("submit", (event)=>{ 
		const title = $("#title").val();
		const content = $("#content").val();
		if(title ==="") {
			alert("Impossible d'avoir un titre d'article vide");
		} else if(content ==="") {
			alert("Impossible d'avoir un contenu d'article vide");
		} else {
		writeArticle(methodes,title, content);
		event.preventDefault();	
	 	event.stopImmediatePropagation();}})
    }
  }, [contract, setEditor])

  return (
    <form>
      <div className={styles.subTitle}>New article</div>
      <div className={styles.mediumWrapper}>
      	<label>Titre de l'article : </label>
        <input id="title"/>
      </div>
      <div className={styles.mediumWrapper}>
      	<label>Contenu de l'article</label>
        <textarea className={styles.editable} id="content"/>
      </div>
      <input type="submit" value="Submit" />
    </form>
  )
}

/**
* Permet d'afficher la page de garde de l'application
*/

const Home = () => {
  return (
    <div className={styles.links}>
      <Link to="/">Home</Link>
      <Link to="/article/new">Add an article</Link>
      <Link to="/article/all">All articles</Link>
    </div>
  )
}


/**
* Fonction utilitaire permettant d'afficher le formulaire de modification d'article sur la page d'un article
*/
function modifyArticleView() {
	$("#articleUpdate").show();
	$("#modify").hide();
}

/**
* Fonction utilitaire appelant smart contrat pour modifier un article
*/
function modifyArticle(methodes, id,  content) {
	methodes.updateArticle(id, content).send();
}

/**
* Un style pour le titre "Contenu précédant" dans la page d'un article
*/
const titleStyle = {
	marginTop: "15px",
	marginBottom: "5px",
	fontSize : "20px",
	textDecoration: "underline"
}


/**
* Permet d'afficher la page d'un article : son titre, son contenu, un formulaire pour modifier le contenu et ses anciens contenus
*/
const Article = () => {
	let { articleId } = useParams();
	const contract = useSelector(({ contract }) => contract)
	

	//On va utiliser le ArticleId pour obtenir l'article et l'afficher
	useEffect(() => {
	    if (contract) {
	      contract.methods.getTitleById(articleId).call().then((value)=> { $("#article").append('<div className="titre" id="title" ><u>'+value+'</u></div>')})
	      contract.methods.articleContent(articleId).call().then((value)=> { $("#article").append('<div className="contenu" id="contentArticle">'+value+'</div>')})	       
	      
	      contract.getPastEvents("Update", {filter : {_articleId: articleId}, fromBlock : 0, toBlock: "latest" }).then(function(events){
	      		events.map( c => $("#previousContent").append('<div className="previously" style="border : 1px solid black">'+c.returnValues._oldContent+'</div>'))
	      
	      });
	      
	    //Pour l'affichage du formulaire de modification  
	      $("#modify").on("click", (event)=>{
		event.preventDefault();	
	 	event.stopImmediatePropagation();
	 	modifyArticleView();})
	 	
	//Pour modifier l'article
	 	const methodes = contract.methods;
  	      $("form").on("submit", (event)=>{
		const content = $("#contentModification").val();
		if(content ==="") {
			alert("Impossible d'avoir un contenu vide");
		} else {
		modifyArticle(methodes, articleId, content); }
		event.preventDefault();	
	 	event.stopImmediatePropagation();})
   	     }
	  }, [contract])
	
          const [editor, setEditor] = useState(null)
  	  useEffect(() => {setEditor(new MediumEditor(`.${styles.editable}`)); 
	      $("#articleUpdate").hide();}, [setEditor]);
	
	return (
	<div id="articlePage">
		<div id="article"> </div>
		<div id="articleUpdate">
			<form>
			      <div className={styles.mediumWrapper}>
			      	<label>Nouveau contenu de l'article : </label>
				<textarea className={styles.editable} id="contentModification"/>
			      </div>
			      <input type="submit" value="Submit" />
			</form>
		</div>
		<button id="modify">Modify Article</button>
		<div id="titlePrevious" style={titleStyle}>Contenu précédant :</div>
		<div id="previousContent"> </div>
	</div>
	)
}

/**
* Permet d'afficher le titre d'un article sur la page /article/all et créer un lien vers sa page d'article
*/
const SimpleArticle = (params) => {
  const [title, setTitle] = useState(null)
  const contract = useSelector(({ contract }) => contract)
  useEffect(() => {
    if (contract) {
      contract.methods.getTitleById(params.name).call().then((value)=> { setTitle(prevTitle => value)} )
    }
  }, [contract, setTitle])
  return (
    <div className={styles.simpleArticle}>
      <Link to={"/article/"+params.name} >{title}</Link>
    </div>
  )
}

/**
* Permet d'afficher la page incluant tous les articles.
*/
const AllArticles = () => {
  const [articles, setArticles] = useState([])
  const contract = useSelector(({ contract }) => contract)
  useEffect(() => {
    if (contract) {
      contract.methods.getAllIds().call().then((value)=> { setArticles(prevArticle => value)} )
    }
  }, [contract, setArticles])
  return <div>{articles.map(article => <SimpleArticle key={article} name={article}/>)}</div>
}

const NotFound = () => {
  return <div>Not found</div>
}

/**
* Permet de créer le routing de l'application. On a simplemnt ajouté un routing vers la page de chaque article par rapport au code fourni.
*/
const App = () => {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(Ethereum.connect)
  }, [dispatch])
  return (
    <div className={styles.app}>
      <div className={styles.title}>Welcome to Decentralized Wikipedia</div>
      <Switch>
        <Route path="/article/new">
          <NewArticle />
        </Route>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/article/all">
          <AllArticles />
        </Route>
        <Route path="/article/:articleId">
          <Article />
        </Route>
        <Route>
          <NotFound />
        </Route>
      </Switch>
    </div>
  )
}

export default App

