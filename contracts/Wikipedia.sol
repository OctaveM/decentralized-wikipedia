pragma solidity ^0.5.0;

contract Wikipedia {
  struct Article {
    string title;
    string content;
  }

  uint nextId;
  uint[] public ids;
  mapping (uint => Article) public articlesById;

  /// @dev Load a first article for the contract
  constructor() public {
    writeArticle("First Article", "This is your first article in your contract");
  }


  function articleContent(uint index) public view returns (string memory) {
    return articlesById[index].content;
  }

  function getAllIds() public view returns (uint[] memory) {
    return ids;
  }
  
  function getTitleById(uint _id) public view returns (string memory) {
    return articlesById[_id].title;
  }

  /// @dev A modifier to show off my newly acquired Solidity skills.
  modifier notEmptyContent (string memory _content) {
  	require(bytes(_content).length > 0);
  	_;
  }

  /// @notice Used to write a new article
  /// @param _title the title for the article
  /// @param _content the body for the article
  function writeArticle(string memory _title, string memory _content) public notEmptyContent(_content) notEmptyContent(_title){
    	ids.push(nextId);
  	articlesById[nextId] = Article(_title, _content);
    	nextId++;
  }
  
  /// @notice Used to update an article
  /// @param _articleId the id of the article that will be updated
  /// @param _newContent the new body for the article
  function updateArticle(uint _articleId, string memory _newContent) public notEmptyContent(_newContent){
  	require(ids[_articleId]==_articleId);//on a bien un article qui correspond à cet id
  	emit Update(_articleId, articlesById[_articleId].content);
  	articlesById[_articleId].content = _newContent;
  }
  
  /// @notice Used to store articles' history
  event Update(uint indexed _articleId, string _oldContent);
}
